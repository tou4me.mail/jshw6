const age = [1, 12, 11, 9, 4, 6];
function Human1() {
  age.sort(function (a, b) {
    if (a > b) {
      return 0;
    } else {
      return -1;
    }
  });
  this.say = document.write(age + '<br><br>');
}
Human1();

// ========================================================================

function Human(name) {
  this.name = name;
  this.message = function () {
    document.write('Hello my name is ' + name + '<br>');
  };
}

function Student(name) {
  this.name = name;
  this.school = function () {
    document.write('I learn JS ' + '<br>');
  };
}

function Worker(name) {
  this.name = name;
  this.say = function () {
    document.write('I want work in front end sphere ' + '<br>');
  };
}

const human = new Human('Denis');
// human.message();

Student.prototype = human;
Worker.prototype = human;

const Den = new Student('Den');
const Deny = new Worker('Deny');

Den.message();
Den.school();
Deny.say();
